/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.cornerstone;

import java.awt.BorderLayout;
import javax.swing.JFrame;

//https://www.youtube.com/watch?v=ByucQN42OOY

/**
 *
 * @author Wayne
 */
public class ProjectCornerstone {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    
        JFrame frame = new JFrame("Platformer");    
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLayout(new BorderLayout() );
        frame.add(new GamePanel(), BorderLayout.CENTER );
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
    }
    
}
